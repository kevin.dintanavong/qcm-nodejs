Comment démarrer l'application ?

- Démarrez votre invité de commande avec les touches WIN+R et taper 'cmd'
- Dirigez-vous vers le chemin du fichier serveur.js via la commande cd
- Ecrivez 'node serveur.js' pour démarrer le serveur
- Lancez désormais votre navigateur préféré, renseignez dans la barre d'adresse 'localhost:2000'
- Une fois sur la page d'accueil, vous pouvez renseigner votre nom et prénom pour accéder au QCM
- Sur ce QCM, il y aura un timer, vous devez répondre aux questions, une seule réponse possible demandé
- Si vous souhaitez upload, redirigez-vous sur cette adresse : localhost:2000/upload

Il est également possible de voir les résultats de chaque candidat dans le dossier upload.
Chaque QCM remplit par un candidat génèrera un fichier .JSON avec ses réponses
